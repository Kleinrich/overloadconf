# OverloadConf overview

OverloadConf is library to ease configuration file loading based on a context.  
The principe is to define common variables (or not, as you want) and contextualized variables (or not, again) in the same file or in a bunch of files.  
This library is able to load a single file, all files in a directory or a bunch or files/directories.

## Installation

The recommended way to install OverloadConf is throught [Composer](https://github.com/composer/composer).
```
  $ composer require kleinrich/overloadConf ~1.0
```

Alternativaly, the code can be clone form this git repotory :
```
  $ git clone https://bitbucket.org/Kleinrich/OverloadConf.git
```

## Configuration files

The library accepts php, json and yml extentions.

Here is an example of a database configuration.
The host is the local machine for all environment but the schema, the user and the password change according to the environment.

Php (database.php) :
```
<?php
use Kleinrich\OverloadConf\OverloadConf;

return array(
    OverloadConf::COMMON => array(
        'host' => 'http://127.0.0.1/',
    ),
    'dev' => array(
        'schema' => 'foo',
        'user' => 'foo',
        'password' => 'foo'
    ),
    'prod' => array(
        'schema' => 'bar',
        'user' => 'bar',
        'password' => 'bar',
    ),
);
```
Json (database.json):
```
{
  "common": {
    "host": "http://127.0.0.1/"
  },
  "dev": {
    "schema": "foo",
    "user": "foo",
    "password": "foo"
  },
  "prod": {
    "schema": "bar",
    "user": "bar",
    "password": "bar"
  }
}
```
Yaml (database.yml):
```
common:
    host: "http://127.0.0.1/"
dev:
    schema: "foo"
    user: "foo"
    password: "foo"
prod:
    schema: "bar"
    user: "bar"
    password: "bar"
```

## Basic usage
OverloadConf works as a service and may (should) be instantiate once. For example, in your DIContainer.

```
<?php
  // May be in your DIContainer
  $oConfigLoader = new \Kleinrich\OverloadConf\OverloadConf();
  $oConfigLoader->setParser(
    new \Kleinrich\OverloadConf\Parser()
  );
```
Then, when you want to get a configuration, you must call the get function of OverloadConf service:
```
<?php
  /**
   * Let's try with the example seen above
   * NB: It may be interresting to create an extention of OverloadConf if your 
   * configuration files are all in the same folder and have the same extention. Then 
   * you will be able to call $oConfigLoader->get('database', $sEnv);
   */
  $sPath = '../config/database.yml';
  /**
   * I choosed the environment, but it could be anything else (lang, country, etc...)
   * It depends on your needs
   */
  $sEnv = 'dev';

  $aConfig = $oConfigLoader->get('./config/database.json', $sEnv);

  $sHost = $aConfig['database.host'];
  $sSchema = $aConfig['database.schema'];
  $sUser = $aConfig['database.user'];
  $sPassword = $aConfig['database.password '];
```

## Advanced Usage
The OverloadConf service is able to load configuration with a [glob](http://man7.org/linux/man-pages/man7/glob.7.html) pattern.
```
<?php
  /**
   * This example will load all JSON files in config folder that begins with database
   * ie : databasedev.json, dabaseprod.json, ...
   */
  $aConfig = $oConfigLoader->get('./config/database*.json', $sContext);
```
It is also able to load all configuration files in a single directory...

```
<?php
  /**
   * In this example, you imagine that database is a folder
   * ie :
   *      -config
   *          -database
   *              -dev.json
   *              -prod.json
   */
  $aConfig = $oConfigLoader->get('./config/database/', $sContext);
```
...or recursively.

```
<?php
  /**
   * In this example, you imagine that database is a folder
   * ie :
   *      -config
   *          -dev
   *              -database.json
   *          -prod
   *              -database.json
   *          -database.json
   */
  $aConfig = $oConfigLoader->get(
      './config',
      $sContext,
      OverloadConf::OPTION_RECURSIVE
  );
```