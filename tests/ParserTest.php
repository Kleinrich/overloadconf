<?php
namespace Kleinrich\OverloadConf\Tests;

use Kleinrich\OverloadConf\Parser;

class ParserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Tested instance
     * @var Parser $oInstance
     */
    protected $oInstance;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->oInstance = new Parser();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        $this->oInstance = null;
    }

    /**
     * Returns a SPLFileInfo Mock
     */
    protected function getSPLFileInfo($sFileName, $sExtension)
    {
        $oMock = $this->getMockBuilder('SPLFileInfo')
            ->disableOriginalConstructor()
            ->setMethods(array('getExtension', 'getRealPath'))
            ->getMock();
        $oMock
            ->method('getExtension')
            ->willReturn($sExtension);
        $oMock
            ->method('getRealPath')
            ->willReturn(sprintf('%s/fixtures/%s.%s', __DIR__, $sFileName, $sExtension));
        return $oMock;
    }

    /**
     * Returns the valid answer for tests
     * @return array
     */
    protected function getTestedArray()
    {
        return array('foo' => 'bar');
    }

    /**
     * Tests a successful php parsing
     */
    public function testSuccessfulPhpParsing()
    {
        $this->assertEquals(
            $this->oInstance->parse($this->getSPLFileInfo('foo', 'php')),
            $this->getTestedArray()
        );
    }

    /**
     * Tests a successful json parsing
     */
    public function testSuccessfulJsonParsing()
    {
        $this->assertEquals(
            $this->oInstance->parse($this->getSPLFileInfo('foo', 'json')),
            $this->getTestedArray()
        );
    }

    /**
     * Tests a successful yml parsing
     */
    public function testSuccessfulYmlParsing()
    {
        $this->assertEquals(
            $this->oInstance->parse($this->getSPLFileInfo('foo', 'yml')),
            $this->getTestedArray()
        );
    }

    /**
     * Tests an unsorpported file extention
     * @expectedException UnexpectedValueException
     */
    public function testThrowExceptionForUnsupportedExtention()
    {
        $this->oInstance->parse($this->getSPLFileInfo(null, 'invalidextention'));
    }

    /**
     * Tests an invalid file format
     * @expectedException LogicException
     */
    public function testInvalidFileFormat()
    {
        $this->oInstance->parse($this->getSPLFileInfo('invalid', 'php'));
    }
}