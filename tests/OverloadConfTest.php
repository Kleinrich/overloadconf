<?php
namespace Kleinrich\OverloadConf\Tests;

use Kleinrich\OverloadConf\OverloadConf;

class OverloadConfTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Tested instance
     * @var Parser $oInstance
     */
    protected $oInstance;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->oInstance = new OverloadConf();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        $this->oInstance = null;
    }

    /**
     * Tests that an exception is raised if the file path is not a string nor an array
     * @expectedException InvalidArgumentException
     */
    public function testGetNotStringNorArrayFilePath()
    {
        $this->oInstance->get(null, null);
    }

    /**
     * Tests to get configuration with string passed as parameter
     */
    public function testGetSuccessfulConfWithStringFilePath()
    {
        $aFileContent = array(
            OverloadConf::COMMON => array(
                'foo' => 'foo',
                'bar' => 'bar',
            ),
            'context' => array(
                'bar' => 'foo'
            )
        );

        $aExpected = array(
            'foo.foo' => 'foo',
            'foo.bar' => 'foo'
        );

        $oParser = $this
                ->getMockBuilder('Kleinrich\OverloadConf\Parser')
                ->setMethods(array('parse'))
                ->getMock();
        $oParser
            ->expects($this->once())
            ->method('parse')
            ->willReturn($aFileContent);

        $this->oInstance->setParser(
            $oParser
        );

        $this->assertEquals($this->oInstance->get(__DIR__.'/fixtures/foo.php', 'context'), $aExpected);
    }

    /**
     * Tests to get configuration with no specific context defined
     */
    public function testGetSuccessfulConfWithoutSpecificContext()
    {
        $aFileContent = array(
            OverloadConf::COMMON => array(
                'foo' => 'foo',
                'bar' => 'bar',
            ),
            'context' => array(
                'bar' => 'foo'
            )
        );

        $aExpected = array(
            'foo.foo' => 'foo',
            'foo.bar' => 'bar',
        );

        $oParser = $this
                ->getMockBuilder('Kleinrich\OverloadConf\Parser')
                ->setMethods(array('parse'))
                ->getMock();
        $oParser
            ->method('parse')
            ->willReturn($aFileContent);

        $this->oInstance->setParser(
            $oParser
        );

        $this->assertEquals($this->oInstance->get(__DIR__.'/fixtures/foo.php', 'undefinedContext'), $aExpected);
    }

    /**
     * Tests to get configuration that contains sub array
     */
    public function testGetSuccessfulConfWithSubArray()
    {
        $aFileContent = array(
            OverloadConf::COMMON => array(
                'foo' => array('bar' => true)
            ),
            'context' => array(
                'foo' => array('bar' => false)
            )
        );

        $aExpected = array(
            'foo.foo' => array('bar' => false)
        );

        $oParser = $this
                ->getMockBuilder('Kleinrich\OverloadConf\Parser')
                ->setMethods(array('parse'))
                ->getMock();
        $oParser
            ->expects($this->once())
            ->method('parse')
            ->willReturn($aFileContent);

        $this->oInstance->setParser(
            $oParser
        );

        $this->assertEquals($this->oInstance->get(__DIR__.'/fixtures/foo.php', 'context'), $aExpected);
    }

    /**
     * Tests to get configuration that contains sub array numerically indexed
     */
    public function testSuccessfulConfWithSubArrayNumericIndex()
    {
        $aFileContent = array(
            OverloadConf::COMMON => array(
                'foo' => array('bar')
            ),
            'context' => array(
                'foo' => array('foo')
            )
        );

        $aExpected = array(
            'foo.foo' => array('bar', 'foo')
        );

        $oParser = $this
                ->getMockBuilder('Kleinrich\OverloadConf\Parser')
                ->setMethods(array('parse'))
                ->getMock();
        $oParser
            ->expects($this->once())
            ->method('parse')
            ->willReturn($aFileContent);

        $this->oInstance->setParser(
            $oParser
        );

        $this->assertEquals($this->oInstance->get(__DIR__.'/fixtures/foo.php', 'context'), $aExpected);
    }

    /**
     * Tests to get configuration with an array of file paths
     */
    public function testGetSuccessfulConfWithArrayFilePath()
    {
        $aFileContent1 = array(
            OverloadConf::COMMON => array(
                'foo' => 'foo',
                'bar' => 'bar',
            ),
            'context' => array(
                'bar' => 'foo'
            )
        );

        $aFileContent2 = array(
            OverloadConf::COMMON => array(
                'foo' => 'foo',
                'bar' => 'bar',
            ),
            'context' => array(
                'bar' => 'foo'
            )
        );

        $aExpected = array(
            'foo.foo' => 'foo',
            'foo.bar' => 'foo',
            'bar.foo' => 'foo',
            'bar.bar' => 'foo',
        );

        $oParser = $this
                ->getMockBuilder('Kleinrich\OverloadConf\Parser')
                ->setMethods(array('parse'))
                ->getMock();
        $oParser
            ->method('parse')
            ->will($this->onConsecutiveCalls($aFileContent1, $aFileContent2));

        $this->oInstance->setParser(
            $oParser
        );

        $this->assertEquals(
            $this->oInstance->get(array(__DIR__.'/fixtures/foo.php', __DIR__.'/fixtures/bar.php'), 'context'),
            $aExpected
        );
    }

    /**
     * Tests to get configuration from a folder (no recurcivity)
     */
    public function testGetSuccessfulConfFromFolder()
    {
        $aFileContent = array(
            OverloadConf::COMMON => array(
                'foo' => 'foo',
                'bar' => 'bar',
            ),
            'context' => array(
                'bar' => 'foo'
            )
        );

        $aExpected = array(
            'bar.foo' => 'foo',
            'bar.bar' => 'foo',
            'foo.foo' => 'foo',
            'foo.bar' => 'foo',
        );

        $oParser = $this
                ->getMockBuilder('Kleinrich\OverloadConf\Parser')
                ->setMethods(array('parse'))
                ->getMock();
        $oParser
            ->method('parse')
            ->willReturn($aFileContent);

        $this->oInstance->setParser(
            $oParser
        );

        $this->assertEquals($this->oInstance->get(__DIR__.'/fixtures/directory', 'context'), $aExpected);
    }

    /**
     * Tests to get configuration from a folder recursively
     */
    public function testGetSuccessfulConfFromFolderRecursively()
    {
        $aFileContent1 = array(
            OverloadConf::COMMON => array(
                'foo' => 'foo',
                'bar' => 'bar',
            ),
            'context' => array(
                'bar' => 'foo'
            )
        );

        $aFileContent2 = array(
            OverloadConf::COMMON => array(
                'foo' => 'foo',
                'bar' => 'bar',
            ),
            'context' => array(
                'bar' => 'bar'
            )
        );

        $aExpected = array(
            'bar.foo' => 'foo',
            'bar.bar' => 'foo',
            'foo.foo' => 'foo',
            'foo.bar' => 'bar',
        );

        $oParser = $this
                ->getMockBuilder('Kleinrich\OverloadConf\Parser')
                ->setMethods(array('parse'))
                ->getMock();
        $oParser
            ->method('parse')
            ->will($this->onConsecutiveCalls($aFileContent1, $aFileContent1, $aFileContent2));

        $this->oInstance->setParser(
            $oParser
        );

        $this->assertEquals(
            $this->oInstance->get(__DIR__.'/fixtures/directory', 'context', OverloadConf::OPTION_RECURSIVE),
            $aExpected
        );
    }
}