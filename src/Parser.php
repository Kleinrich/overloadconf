<?php
namespace Kleinrich\OverloadConf;

use SplFileInfo;
use UnexpectedValueException;
use LogicException;
use Symfony\Component\Yaml\Yaml;

class Parser
{
    /**
     * Map the file extensions to functions
     * @var array $aExtentionMap
     */
    protected $aExtentionMap = array(
        'php' => 'parsePhp',
        'json' => 'parseJson',
        'yml' => 'parseYml',
    );

    /**
     * Parses a file and returns its content
     * @return array
     * @throws UnexpectedValueException if the extention is not supported
     * @throws LogicException if the file is not well formatted
     */
    public function parse(SplFileInfo $oFile)
    {
        if (isset($this->aExtentionMap[$oFile->getExtension()]) === false) {
            throw new UnexpectedValueException(sprintf('Unsupported file extention : %s', $oFile->getExtension()));
        }

        $aReturn = $this->{$this->aExtentionMap[$oFile->getExtension()]}($oFile);
        if (is_array($aReturn) === false) {
            throw new LogicException(sprintf('%s is not a well formatted configuration file', $oFile->getFileName()));
        }

        return $aReturn;
    }

    /**
     * Parses a PHP file and returns its content
     * @return mixed
     */
    protected function parsePhp(SplFileInfo $oFile)
    {
        return include($oFile->getRealPath());
    }

    /**
     * Parses a JSON file and returns its content
     * @return mixed
     */
    protected function parseJson(SplFileInfo $oFile)
    {
        return json_decode(file_get_contents($oFile->getRealPath()), true);
    }

    /**
     * Parses a YML file and returns its content
     * @return mixed
     */
    protected function parseYml(SplFileInfo $oFile)
    {
        return Yaml::parse(file_get_contents($oFile->getRealPath()));
    }
}