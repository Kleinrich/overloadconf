<?php
namespace Kleinrich\OverloadConf;

use GlobIterator;
use UnexpectedValueException;
use InvalidArgumentException;

class OverloadConf
{
    /**
     * Common configuration index
     * @var string COMMON
     */
    const COMMON = 'common';

    /**
     * Options : Indicates if the directory must be loaded recursively
     * @var string COMMON
     */
    const OPTION_RECURSIVE = 1;

    /**
     * Content parser
     * @var Parser $oParser
     */
    protected $oParser;

    /**
     * @see OverloadConf::$oParser
     * @param Parser $oParser
     * @return OverloadConf
     */
    public function setParser(Parser $oParser)
    {
        $this->oParser = $oParser;

        return $this;
    }

    /**
     * Returns the configuration according to the file path(s), the context, and the options
     * @param array|string $mFilePath Path to a file/directory or array of path to files or directories 
     * @param string $sContext The current environment
     * @param boolean $bRecursive If the config must be loaded recursively (won't affect simple file loading)
     * @throws InvalidArgumentException If first parameter is not a string neither an array  
     */
    public function get($mFilePaths, $sContext, $iOptions = 0)
    {
        if (is_string($mFilePaths) === false && is_array($mFilePaths) === false) {
            throw new InvalidArgumentException(
                sprintf('First parameter must be a string or an array, %s given', gettype($mFilePaths))
            );
        }

        $aOutput = array();
        foreach ($this->buildFinalPaths($mFilePaths) as $sFilePath) {
           $this->hydrate($aOutput, $sFilePath, $sContext, $iOptions); 
        }

        return $aOutput;
    }

    /**
     * Builds an array of real paths according to the entry
     * The entry may be a string or an array of string
     * @param array|string $mFilePaths
     * @return array
     */
    protected function buildFinalPaths($mFilePaths)
    {
        $aFiles = is_string($mFilePaths) === true ? array($mFilePaths) : $mFilePaths;
        $aFiles = array_map(
            function($sPattern) {
                return array_map(
                    function($sPath) {
                        return realpath($sPath);
                    },
                    glob($sPattern, GLOB_MARK)
                );
            },
            $aFiles
        );
        
        $aFinalPaths = array();
        foreach ($aFiles as $aFilesPath) {
            foreach($aFilesPath as $sFilePath) {
               $aFinalPaths[] = $sFilePath;
            }
        }

        return $aFinalPaths;
    }

    /**
     * Hydrates the current object with the config loaded from the file path
     * @param string $sFilePath Path to the file
     */
    protected function hydrate(&$aOutput, $sFilePath, $sContext, $iOptions = 0, $iDetph = 0)
    {
        $oGlob = new GlobIterator($sFilePath);
        foreach ($oGlob as $oFile) {
            if ($oFile->isDir() === true) {
                if ($iDetph === 0 || $iOptions & static::OPTION_RECURSIVE) {
                    $this->hydrate($aOutput, $oFile->getPathname().DIRECTORY_SEPARATOR.'*', $sContext, $iOptions, ++$iDetph);
                }
            } else {
                $this->setSolidConfig(
                    $aOutput,
                    $oFile->getBasename(
                        sprintf('.%s',$oFile->getExtension())
                    ),
                    $this->oParser->parse($oFile),
                    $sContext,
                    $iOptions
                ); 
            }                
        }
    }

    /**
     * Transforms the configuration array to attributes
     * @param string $sFileName The origin filename of the configuration
     * @param array $aConfig The configuration array
     */
    protected function setSolidConfig(array &$aOutput, $sFileName, array $aConfig, $sContext, $iOptions = 0)
    {
        $aFinalConfig = isset($aConfig[static::COMMON]) === true ? $aConfig[static::COMMON] : array();

        if (isset($aConfig[$sContext]) === true) {
            $aFinalConfig = $this->fixedArrayMergeRecursive($aFinalConfig, $aConfig[$sContext]);
        }

        foreach ($aFinalConfig as $sKey => $mValue) {
            $aOutput[sprintf('%s.%s', $sFileName, $sKey)] = $mValue;
        }
    }

    /**
     * The native array_merge_recursive function may transforms some value into arrays by mistake
     * example :
     *    array_merge_recursive(array('key' => false), array('key' => true));
     *  will produce :
     *    array('key' => array(false, true))
     *  But we expected the last value to erase the previous one :
     *    array('key' => true)
     * @param *polymorph* expects array
     * @return array
     */
    protected function fixedArrayMergeRecursive()
    {
        $aArrays = func_get_args();

        $aMerge = array_shift($aArrays);

        foreach ($aArrays as $aArray) {
            foreach ($aArray as $sKey => $val) {
                if (is_array($val) && array_key_exists($sKey, $aMerge)) {
                    $val = $this->fixedArrayMergeRecursive((array) $aMerge[$sKey], $val);
                }

                if (is_numeric($sKey)) {
                    $aMerge[] = $val;
                } else {
                    $aMerge[$sKey] = $val;
                }
            }
        }

        return $aMerge;
    }
}